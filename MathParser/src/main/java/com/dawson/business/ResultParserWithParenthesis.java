package com.dawson.business;

import com.dawson.exceptions.DivisionByZero;
import com.dawson.exceptions.InvalidInputQueueString;
import com.dawson.exceptions.NonBinaryExpression;
import com.dawson.exceptions.NonMatchingParenthesis;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Queue;

/**
 *
 * @author Simranjot Kaur Khera
 */
public class ResultParserWithParenthesis {

    HashMap<String, Integer> operatorPrecedence = new HashMap<String, Integer>();

    public ResultParserWithParenthesis() {
        this.operatorPrecedence.put("/", 2);
        this.operatorPrecedence.put("*", 2);
        this.operatorPrecedence.put("-", 1);
        this.operatorPrecedence.put("+", 1);

    }

    /**
     * Converts an infix queue to a postfix queue using helper methods. It loops
     * through the infixQueue and checks what each element is. Depending on the
     * element, different checks are performed and then it is added to either
     * the stack or the postfixQueue. In the end, the stack is emptied of its
     * operators and added to the final version of the postfix queue.
     *
     * @param infixQueue
     * @return
     * @throws NonMatchingParenthesis
     * @throws InvalidInputQueueString
     * @throws NonBinaryExpression
     */
    public Queue<String> convertInfixToPostfix(Queue<String> infixQueue) throws NonMatchingParenthesis, InvalidInputQueueString, NonBinaryExpression {

        this.transformInfixQueue(infixQueue);
        this.checkMatchingParenthesis(infixQueue);

        Queue<String> postfixQueue = new ArrayDeque<>();
        Deque<String> operatorStack = new ArrayDeque<>();

        while (!infixQueue.isEmpty()) {
            // removing and getting first element.
            String currentElement = infixQueue.remove().trim();

            if (this.operatorPrecedence.containsKey(currentElement)) {
                this.processOperatorElement(infixQueue, postfixQueue, operatorStack, currentElement);
            } else if (currentElement.equals("(")) {
                this.validateForNextParenthesis(infixQueue);
                operatorStack.push(currentElement);
            } else if (currentElement.equals(")")) {
                this.emptyUntilClosingParenthesis(operatorStack, postfixQueue);
            } else {
                this.processNumberElement(infixQueue, currentElement, postfixQueue);
            }
        }

        //adding the rest of the operators from stack to queue.
        while (!operatorStack.isEmpty()) {
            postfixQueue.add(operatorStack.pop());
        }
        return postfixQueue;
    }

    /**
     * Goes through the postfixQueue, parses numbers into a stack until an
     * operator is reached. Operator is popped and used on the first 2 elements
     * in the operand stack. An operator is reached when parsing of the element
     * to double fails.
     *
     * @param postfixQueue
     * @return
     * @throws DivisionByZero
     */
    public String evaluatePostfix(Queue<String> postfixQueue) throws DivisionByZero {
        Deque<Double> operandStack = new ArrayDeque<>();
        Double firstOperand = 0.0;
        Double secondOperand = 0.0;
        for (String currentElement : postfixQueue) {
            try {
                //keep parsing and storing into stack until operator is reached. 
                operandStack.push(Double.parseDouble(currentElement));
            } catch (NumberFormatException ex) {

                secondOperand = operandStack.pop();
                firstOperand = operandStack.pop();

                if (currentElement.equals("+")) {
                    operandStack.push(firstOperand + secondOperand);
                }

                if (currentElement.equals("-")) {
                    operandStack.push(firstOperand - secondOperand);
                }

                if (currentElement.equals("*")) {
                    operandStack.push(firstOperand * secondOperand);
                }

                //check for division by 0 before dividing. 
                if (currentElement.equals("/")) {
                    if (secondOperand == 0.0) {
                        throw new DivisionByZero("Cannot divide by zero");
                    } else {
                        operandStack.push(firstOperand / secondOperand);
                    }
                }
            }
        }

        return operandStack.pop().toString();
    }

    /**
     * Changes infixQueue to a transformed queue. This method adds "*" between
     * numbers and parenthesis as well as between 2 parentheses. It also changes
     * -( and +( to -1*( and ( respectively.
     *
     * @param infixQueue
     */
    private void transformInfixQueue(Queue<String> infixQueue) {
        Queue<String> transformedQueue = new ArrayDeque<>();

        while (infixQueue.size() > 1) {
            String previous = infixQueue.remove().trim();
            Boolean previousAdded = false;

            //"-(" is equivilent to -1*(
            if (previous.equals("-(")) {
                transformedQueue.add("-1");
                transformedQueue.add("*");
                transformedQueue.add("(");
                continue;
            }

            //"+(" is equivilent to "("
            if (previous.equals("+(")) {
                transformedQueue.add("(");
                continue;
            }
            //Add "*" between ")" and "(" -> emplies multiplication
            if (previous.equals(")") && infixQueue.peek().equals("(")) {
                this.addOperator(transformedQueue, previous);
                continue;
            }

            //if previous is a number and next is a "(" -> add "*" between them, emplies multiplication
            if (infixQueue.peek().equals("(")) {
                try {
                    Double.parseDouble(previous);
                    previousAdded = this.addOperator(transformedQueue, previous);
                } catch (NumberFormatException ex) {
                    System.out.println("Not a number before opening parenthese");
                }
            }

            //if next element is a number -> add "*" between parenthesis and number, emplies multiplication
            if (previous.equals(")")) {
                String next = infixQueue.peek();
                try {
                    Double.parseDouble(next);
                    previousAdded = this.addOperator(transformedQueue, previous);
                } catch (NumberFormatException ex) {
                    System.out.println("Not a number after closing parenthese");
                }

            }

            //adding elements that don't require checks.
            if (!previousAdded) {
                transformedQueue.add(previous);
            }
        }
        //adding the last element to transformedQueue
        transformedQueue.add(infixQueue.remove());

        //changing infixQueue to the transformedQueue
        for (String e : transformedQueue) {
            infixQueue.add(e);
        }
    }

    /**
     * Compares numbers of opening and closing parentheses. Throws error if they
     * don't match.
     *
     * @param infixQueue
     * @throws NonMatchingParenthesis
     */
    private void checkMatchingParenthesis(Queue<String> infixQueue) throws NonMatchingParenthesis {
        int openingCount = Collections.frequency(infixQueue, "(");
        int closingCount = Collections.frequency(infixQueue, ")");

        if (openingCount != closingCount) {
            throw new NonMatchingParenthesis("Number of parenthesis don't match");
        }
    }

    /**
     * This makes sure there is an operator after every operand.
     *
     * @param infixQueue
     * @throws NonBinaryExpression
     */
    private void validateForNextNumber(Queue<String> infixQueue) throws NonBinaryExpression {
        if (infixQueue.size() > 1 && !infixQueue.peek().equals(")")) {
            if (!this.operatorPrecedence.containsKey(infixQueue.peek())) {
                throw new NonBinaryExpression("Need operator after number");
            }
        }

    }

    /**
     * When an operator is found, the next element needs to be a number. This
     * method makes sure that an operator has no missing operand.
     *
     * @param infixQueue
     * @throws NonBinaryExpression
     * @throws InvalidInputQueueString
     */
    private void validateForNextOperator(Queue<String> infixQueue) throws NonBinaryExpression, InvalidInputQueueString {
        String next = infixQueue.peek();
        //for situations like -> "3","/" -> missing operand.
        if (infixQueue.isEmpty()) {
            throw new NonBinaryExpression("Number needed after op");
        }

        if (!next.equals("(")) {
            try {
                Double.parseDouble(next);
            } catch (NumberFormatException ex) {
                throw new InvalidInputQueueString("need a number after operator");
            }
        }
    }

    /**
     * Checks for empty parentheses.
     *
     * @param infixQueue
     * @throws NonBinaryExpression
     */
    private void validateForNextParenthesis(Queue<String> infixQueue) throws NonBinaryExpression {
        if (infixQueue.peek().equals(")")) {
            throw new NonBinaryExpression("Empty parenthesis");
        }

    }

    /**
     * When an operator is found while solving while making postfixQueue, we
     * need to compare currentOperator with the next operator in the stack and
     * swap if necessary.
     *
     * @param infixQueue
     * @param postfixQueue
     * @param operatorStack
     * @param currentElement
     * @throws NonBinaryExpression
     * @throws InvalidInputQueueString
     */
    private void processOperatorElement(Queue<String> infixQueue, Queue<String> postfixQueue, Deque<String> operatorStack, String currentElement) throws NonBinaryExpression, InvalidInputQueueString {
        this.validateForNextOperator(infixQueue);
        //if the operator stack is not empty -> compare operators on top of stack with currentElement.
        //Do swap depending on result of comparison. 
        if (!operatorStack.isEmpty()) {
            //we dont want to compare operators if the top of stack contains "(" -> we add operator immediately on top.
            if (!operatorStack.peek().equals("(")) {
                //if currentOperator is less than stack peek -> pop and replace
                if (this.operatorPrecedence.get(currentElement) <= this.operatorPrecedence.get(operatorStack.peek())) {
                    String higherOperator = operatorStack.pop();
                    postfixQueue.add(higherOperator);
                }
            }
        }
        //if the element is an operator we add it to operator stack no matter what.
        operatorStack.push(currentElement);
    }

    /**
     * Validate, parse and add the current number into the postfixQueue.
     *
     * @param infixQueue
     * @param currentElement
     * @param postfixQueue
     * @throws NonBinaryExpression
     * @throws InvalidInputQueueString
     */
    private void processNumberElement(Queue<String> infixQueue, String currentElement, Queue<String> postfixQueue) throws NonBinaryExpression, InvalidInputQueueString {
        this.validateForNextNumber(infixQueue);
        try {
            Double.parseDouble(currentElement);
            postfixQueue.add(currentElement);
        } catch (NumberFormatException ex) {
            throw new InvalidInputQueueString("Invalid number, cannot be parsed to double");
        }
    }

    /**
     * When a closing parenthesis is found, we need to pop everything from stack
     * and add them to the queue until "(" if found.
     *
     * @param operatorStack
     * @param postfixQueue
     */
    private void emptyUntilClosingParenthesis(Deque<String> operatorStack, Queue<String> postfixQueue) {
        while (!operatorStack.peek().equals("(")) {
            postfixQueue.add(operatorStack.pop());
        }
        //removing ( from stack -> we don not add the parentheses in the postfixQueue
        operatorStack.pop();
    }

    /**
     * When transforming the infixQueue, we need to add "*" between different
     * elements.
     *
     * @param transformedQueue
     * @param previous
     * @return
     * @see transformInfixQueue
     */
    private Boolean addOperator(Queue<String> transformedQueue, String previous) {
        transformedQueue.add(previous);
        transformedQueue.add("*");
        return true;
    }

}
