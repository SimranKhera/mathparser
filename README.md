# MathParser

What does it do?

    Given a mathematical equation (using command line), gives an answer based on the order of precedence.

What's the focus?

    The focus of this project is to write business level test cases by implementing best practices.
